from django.contrib import admin
from django.contrib.auth.models import Permission
from .models import ScrumyGoals, ScrumHistory, GoalStatus

# Register your models here.
admin.site.register(ScrumyGoals)
admin.site.register(ScrumHistory)
admin.site.register(GoalStatus)
admin.site.register(Permission)

