from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class GoalStatus(models.Model):
  status_name = models.CharField(max_length=50)

  def __str__(self):
    return self.status_name


class ScrumyGoals(models.Model):
  goal_name = models.CharField(max_length=50)
  goal_id = models.IntegerField()
  created_by = models.CharField(max_length=50, null=True)
  moved_by = models.CharField(max_length=50, null=True)
  owner = models.CharField(max_length=50, null=True)
  goal_status=models.ForeignKey(GoalStatus, on_delete = models.PROTECT)
  user=models.ForeignKey(User, on_delete = models.PROTECT, related_name='user_goal')

  def __str__(self):
    return self.goal_name


class ScrumHistory(models.Model):
  moved_by = models.CharField(max_length=50)
  created_by = models.CharField(max_length=50)
  moved_from = models.CharField(max_length=50)
  moved_to = models.CharField(max_length=50)
  time_of_action = models.DateField(auto_now=True);
  goal = models.ForeignKey(ScrumyGoals, on_delete=models.PROTECT)

  def __str__(self):
    return self.created_by
