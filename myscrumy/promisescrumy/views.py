from django.shortcuts import render, redirect
from random import randint
from django.http import HttpResponse
from django.contrib.auth.models import User, Group
from django.contrib.auth.forms import UserCreationForm
from .models import ScrumyGoals, GoalStatus, ScrumyGoals
from .forms import CreateGoalForm, MoveGoalForm, AdminCreateGoalForm

# Create your views here.

def index(request):
  form = UserCreationForm()
  if request.method == "POST":
    form = UserCreationForm(request.POST)
    if form.is_valid():
      user = form.save()
      dev_group = Group.objects.get(name="Developer")
      dev_group.user_set.add(user)
      return redirect('success')
  else:
    form = UserCreationForm()
  return render(request, 'promisescrumy/index.html', {'form' : form})

def success(request):
  return render(request, 'promisescrumy/success.html')

def home(request):
  #scrumy_goal = ScrumyGoals.objects.get(goal_name="Learn Django")
  all_users = User.objects.all()
  context = {
    'all_users' : all_users,
  }
  return render(request, 'promisescrumy/home.html', context)

def add_goal(request):
  if 'Admin' in str(request.user.groups.all()):
    form = AdminCreateGoalForm()
  else:
    form = CreateGoalForm()
  if request.method == "POST":
    goal_id = randint(9999, 10000)
    if ScrumyGoals.objects.filter(goal_id=goal_id):
      goal_id = randint(999, 10000)
    
    if 'Admin' in str(request.user.groups.all()):
      form = AdminCreateGoalForm(request.POST)
      if form.is_valid():
        form.save()
        return redirect('index')

    else:
      form = CreateGoalForm(request.POST)
      if form.is_valid():
        goal = form.save(commit=False)
        goal.goal_id = goal_id 
        goal.goal_status = GoalStatus.objects.get(status_name='Weekly Goal')
        goal.user = request.user
        goal.save()
        return redirect('index')
    
  else:
    form = CreateGoalForm()
  return render(request, 'promisescrumy/add_goal.html', {'form' : form})

def move_goal(request, goal_id):
  try:
    obj = ScrumyGoals.objects.get(goal_id=goal_id)
    form = MoveGoalForm(instance=obj)
    error = []
    set = 'Hello';
    if request.method == "POST":
      form = MoveGoalForm(request.POST, instance=obj)
      if form.is_valid():
        if 'Developer' in str(request.user.groups.all()) and str(form.cleaned_data['goal_status']) == 'Done Goal':
          form = MoveGoalForm(request.POST, instance=obj)
          error.append('You are not allowed to perform that operation')
          return render(request, 'promisescrumy/move_goal.html', {'form':form, 'error': error})
        if 'Quality Analyst' in str(request.user.groups.all()) and obj.user != request.user and str(form.cleaned_data['goal_status']) != 'Done Goal':
          error.append('You are not allowed to perform that operation')
          form = MoveGoalForm(request.POST, instance=obj)
          return render(request, 'promisescrumy/move_goal.html', {'form':form, 'error': error})
        form.save()
        return redirect('index')

    else:
      form = MoveGoalForm(instance=obj)
  except Exception as e:
    return render(request, 'promisescrumy/exception.html', {'error':'A record with that goal id does not exist'})
  else:
    return render(request, 'promisescrumy/move_goal.html', {'form':form})

