from django import forms
from django.contrib.auth.models import User
from .models import ScrumyGoals, GoalStatus

class MoveGoalForm(forms.ModelForm):

  class Meta:
    model = ScrumyGoals
    fields = ['goal_status']

class AdminCreateGoalForm(forms.ModelForm):

  class Meta:
    model = ScrumyGoals
    fields = ['user', 'goal_name']

class CreateGoalForm(forms.ModelForm):

  class Meta:
    model = ScrumyGoals
    fields = ['goal_name']