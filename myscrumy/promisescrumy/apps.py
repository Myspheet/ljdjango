from django.apps import AppConfig


class PromisescrumyConfig(AppConfig):
    name = 'promisescrumy'
